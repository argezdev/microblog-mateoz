# Welcome to Wellfound's microblog assessment!

## Goal
Your task is to update the home page of this Flask app to display all of the rows in the blog posts table instead of just the static single row. 

1. To solve this we go to app/templates/index.html

- We initially are given a static post 
    
(<span id="post1">This is my one and only blog post. Can you add more using a for loop?</span>) 
-using a span

2.

- We simply need to retrieve the posts from the template (They are retrieved on _post.html which creates the same span component as the previous static component but for each one of the posts created on the routes.py file, app/main/routes.py , in there we can create as many posts as needed, added an extra one for testing and for more validation to this assesment.)

Clone/fork this repository and make a commit with this change. After you've made your changes, please take a screenshot of the updated home page and include it in the README of your public repository. Finally, email the link to your public repository to clayton.bridge@wellfound.co, and please include your full name in the email!

<BR><BR>


<h1>Screenshot of homepage</h1>
<img
  src="WFSC.png"
  alt="Alt text"
  title="Optional title"
  style="display: inline-block; margin: 0 auto; max-width: 600px">

<BR><BR>

## Contributions
Feel free to clone this repo locally and use your own editor of choice.

If you would rather use the Gitpod development environment for this app:

- Change the dropdown that says "Web IDE" to "Gitpod" (if it already says "Gitpod" skip this step)
- Click the button that says "Gitpod"
